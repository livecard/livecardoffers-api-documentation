# LiveCardOffers API Documentation #

### Login ###

GET /amember/api/check-access/by-login-pass?_key=<amember-api-key>&login=<username>&pass=<password>

```
curl http://livecardoffers.com/amember/api/check-access/by-login-pass?_key=6rVOC1GrN5wgz3dsidnd&login=test&pass=secret
```
** Returns: **
```
{"ok":true,"user_id":441,"name":"Test Test","name_f":"Test","name_l":"Test","email":"test@test.com","login":"test"}
```

### To get a list of offers ###

GET /wp-json/wp/v2/job_listing

```
curl http://livecardoffers.com/wp-json/wp/v2/job_listing
```
 
 
### Append ?page=<number> to get next page ###

GET /wp-json/wp/v2/job_listing?page=<number>

```
curl http://livecardoffers.com/wp-json/wp/v2/job_listing?page=2
```
Total number of offers and pages are returned in the HTTP header as X-WP-Total and X-WP-TotalPages.


### To get offers near a location with latitude and longitude ###

GET /wp-json/livecard/v1/offers?lat=<latitude>&lng=<longitude>

```
curl http://livecardoffers.com/wp-json/livecard/v1/offers?lat=36.7468422&lng=-119.7725868
```

### To get an individual offer ###

GET /wp-json/wp/v2/job_listing/<post_id>

```
curl http://livecardoffers.com/wp-json/wp/v2/job_listing/3031
```

### To get image for that offer (_thumbnail_id is listed in the individual offer json) ###

GET /wp-json/wp/v2/media/<thumbnail_id>


```
curl http://livecardoffers.com/wp-json/wp/v2/media/1508
```


### To get redemption info for an offer ###

GET /wp-json/livecard/v1/offer/<post_id>/redeem/<redeemption_method>?user_id=<user_id>&first_name=<first_name>&last_name=<last_name>

** PARAMETERS **:

* <post_id> (required)

* <redemption_method> (required, either "link", "instore", "instore_print", or "call", available redemption methods are listed in the individual offer json)

* <user_id> (required, returned by Login endpoint above)

* <first_name> (required, user's first name, returned by Login endpoint above)

* <last_name> (required, user's last name, returned by Login endpoint above)


```
curl http://livecardoffers.com/wp-json/livecard/v1/offer/11690/redeem/instore?user_id=1&first_name=Bob&last_name=Smith
```

** Returns ** (details.link contains coupon url):
```
{
    redemption_method: "instore",
    content_type: "text/html",
    details: {
        link: "http://static-demo.accessdevelopment.com/temp/coupon/6efc6c12201afa869cf885854288219ff7af4d94/original.html"
    },
    links: [ ]
}
```